#!/bin/env python

import sys
from os import path

sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))

from pssapi.utils import decor
from recycle import recycler

def _app_terminated(*args):
    """Log message to the logs stating this app was terminated."""
    recycler.quit_script("[FATAL]", "Application was terminated.")

@decor.onterminated(_app_terminated)
def main():
    # script must be called passing in a parameter: 
    # file to be processed, which matches the name of a brand/app/frontend/...
    if len(sys.argv) != 2:
        err_msg = "Wrong number of parameters."
        try:
            recycler.quit_script("[MISSING]", err_msg)
        except:
            print("ERROR: " + err_msg)
            exit()
    recycler.start(sys.argv)
    recycler.recycle(sys.argv[1])

if __name__ == "__main__":
    main()
