"""
Bounces servers.

Usage:
./recycler <component-file-name>

'component-file-name' is the name of the json file (without the extension) 
this app will use to restart the servers defined in it. File must be located 
in 'components' folder:

            .
            ..
            recycler.py
            conf/
                conf.json
                components/
                        |---- sf-tst.json
                        |---- cbo-prod.json
                        |---- sf-ldt.json

So for recycling SF servers in TST, according to above directory structure, 
this app should be called this way:
    ./recycler.py sf-tst

Multiple servers can be bounced simultaneously by including them on 
the same list in the json under the key 'servers'. For every list of 
server names, this app will create a thread.

List of the servers can be defined as the name of a variable in a bash script, 
in which case, instead of defining a list in the conf file, a dictionary is 
expected. The key of the dictionary is the name of the variable, and the 
value in the dictionary is the complete path to the bash script.

The way of stopping/starting/getting status of the servers is defined on
the configuration file by defining the port in which the application is 
listening, and the commands/scripts for executing such actions.
"""

from datetime import datetime
from multiprocessing.pool import ThreadPool as Pool
from os import path
import sys
from time import sleep

from pssapi.cmdexe import cmd
from pssapi.logger.logger import load_logger
from pssapi.utils import conf, conn, util

logger = None
not_stopped_servers = None
not_started_servers = None
processed_servers = None

EXEC_DIR = path.dirname(path.realpath(sys.argv[0]))
LOG_FILE_NAME = path.join(EXEC_DIR, 'logs', '{0}.log')
APP_CONF_FILE_DIR = path.join(EXEC_DIR, 'conf', 'conf.json')
COMP_CONF_FILE_DIR = path.join(EXEC_DIR, 'conf', 'components')

DEFAULT_STOP_ACTION = "kill -9"

def start(args):
    """Load the logger object and print initial text to the log."""
    global logger
    global not_started_servers
    global not_stopped_servers
    global processed_servers
    not_started_servers = []
    not_stopped_servers = []
    processed_servers = []
    logger_fmtter = {
        'INFO': '%(asctime)s - %(thread)s - %(message)s',
        'DEFAULT': '%(asctime)s - %(thread)s - %(levelname)s - %(message)s'
    }
    logfilename = LOG_FILE_NAME.format(args[1])
    logger = load_logger(log_file_name=logfilename, **logger_fmtter)
    logger.info('Recycler is being called with param(s): {0}'.format(args))

def quit_script(component, error_message=None):
    """Print error_message into log file. Quit current script."""
    if error_message:
        logger.critical("Component: {0}. {1}".format(component, error_message))
    logger.info("Component: {0}. Quiting now... Bye!\n".format(component))
    exit()

def _check_configuration(component, comp_conf):
    """Read from 'comp_conf' dictionary containing keys:values for 'component'.
    Check every key is present and every value is correct (at least 
    syntactically).
    """
    errmessage = ""
    conf_keys = ('process_name', 'port', 'actions', 'servers')
    if not set(conf_keys) <= set(comp_conf):
        quit_script(component, "At least one key and/or value " + 
                    "is not present on the conf file")
    try:
        servers = comp_conf['servers']
        actions = comp_conf['actions']
        start = actions['start']
        if not isinstance(servers, list):
            errmessage = "'servers' value should be a list."
        elif not isinstance(actions, dict):
            errmessage = "'actions' value should be a dict."
        elif not start:
            errmessage = "'start' value should be specified."
    except:
        errmessage = "There is a mandatory key missing in the conf file."
    if errmessage:
        quit_script(component, errmessage)

def _get_component_configuration(component):
    """Read from <component>.json file and create a dict with the content."""
    comp_file_path = path.join(COMP_CONF_FILE_DIR, "{0}.json".format(component))
    if not path.isfile(comp_file_path):
        quit_script(component, "No conf file was found for this component.")
    return conf.get_conf(comp_file_path)

def _search_pid_by_name(server, process_name):
    """Search and return the PID by its name in remote server."""
    command = "ssh {0} \"ps -ef | grep {1} | grep -v grep\"".format(server, 
                                                             process_name)
    try:
        stdout, dummy = cmd.cmdexec(command)
        if stdout:
            result = [i for i in stdout.split(' ') if i]
            return result[1]
    except Exception as e:
        logger.error("Searching PID for {0}@{1}: {2}".\
                    format(process_name, server, str(e)))
    return 0

def _get_stopping_command(server, process_name, actions):
    """Return the command defined for stopping process_name in server."""
    if 'stop' in actions and actions['stop']:
        command = actions['stop']
    else:
        msg = "No command was specified for stopping {0},".format(process_name)
        pid = _search_pid_by_name(server, process_name)
        # no pid found... just try again until max attempts are reached
        if pid == 0:
            command = ""
            msg += ((" and no PID was found in {0} for such process").\
                    format(server))
        else:
            command = "{0} {1}".format(DEFAULT_STOP_ACTION, pid)
            msg += ((" so 'kill' command will be used in server {0}.").\
                    format(server))
        logger.warning(msg)
    return command

def _kill_server(server, comp_conf):
    """Kill 'server' running on specific port (comp_conf['port'])."""
    pn = comp_conf['process_name']
    logger.warning("Trying to kill {0}@{1}...".format(pn, server))
    errmsg = "{0}@{1} couldn't be killed: ".format(pn, server)
    if 'finally_kill' in comp_conf['actions'] and \
            comp_conf['actions']['finally_kill']:
        pid = _search_pid_by_name(server, pn)
        if pid != 0:
            command = ("ssh {0} \"{1} {2}\"").\
                    format(server, DEFAULT_STOP_ACTION, pid)
            logger.warning(("Running this command on {0}: {1}...").\
                            format(server, command))
            dummy, errout = cmd.cmdexec(command)
            errmsg += errout
            port = comp_conf['port']
            if not conn.is_port_open(server, port):
                logger.info("{0}@{1} was killed!!".format(pn, server))
                return True
        else:
            errmsg += "process name was not found running."
    else:
        errmsg += ("'finally_kill' key is missing on the component conf " + 
                    "file, or it's defined as 'False'.")
    logger.warning(errmsg)
    return False

def _stop_server(server, comp_conf, app_conf):
    """Stop server and check it is down."""
    process_name = comp_conf['process_name']
    stop_ch = int(app_conf['stop_checking_interval_sec'])
    stop_attempts = int(app_conf['stop_max_attempts'])
    actions = comp_conf['actions']
    port = comp_conf['port']
    logger.info("(stopping) Checking status of {0}@{1}".format(process_name, 
                                                               server))
    if conn.is_port_open(server, port):
        logger.info(("(stopping) {0}@{1} is running; " + 
                     "trying to stop it down.").format(process_name, server))
        command = _get_stopping_command(server, process_name, actions)
        if not command:
            return False
        command = "ssh {0} \"{1}\"".format(server, command)
        cmd.cmdexec(command)
        for i in range(stop_attempts):
            sleep(stop_ch)
            logger.info(("(stopping) Checking status of {0}@{1} " + 
                         "(attempt: {2}/{3}).").\
                        format(process_name, server, i + 1, stop_attempts))
            if conn.is_port_open(server, port):
                msg = ("(stopping) {0}@{1} is still running. ").\
                      format(process_name, server)
                if i != (stop_attempts - 1): # not last attempt
                    msg += (("Waiting {0} seconds to check again.").\
                            format(stop_ch))
                    logger.info(msg)
                else: # last attemp, server was not stopped
                    msg += (("Max attempts reached ({0}))!!").\
                            format(stop_attempts))
                    logger.warning(msg)
                    return False
            else: # server was stopped
                logger.info(("(stopping) {0}@{1} was brought down.").\
                            format(process_name, server))
                return True
    else:
        logger.info("(stopping) {0}@{1} was already down.".\
                    format(process_name, server))
        return True
    return False

def _start_server(server, comp_conf, app_conf):
    """Start server and check it is up and running."""
    process_name = comp_conf['process_name']
    command = "ssh {0} \"{1}\"".format(server, comp_conf['actions']['start'])
    start_ch = int(app_conf['start_checking_interval_sec'])
    start_attempts = int(app_conf['start_max_attempts'])
    port = comp_conf['port']
    logger.info("(starting) Checking status of {0}@{1}".format(process_name, 
                                                               server))
    if not conn.is_port_open(server, port):
        logger.info(("(starting) {0}@{1} is not running; " + 
                     "trying to start it up.").format(process_name, server))
        cmd.cmdexec(command)
        for i in range(start_attempts):
            sleep(start_ch)
            logger.info(("(starting) Checking status of {0}@{1} " + 
                         "(attempt: {2}/{3}).").\
                        format(process_name, server, i + 1, start_attempts))
            if not conn.is_port_open(server, port):
                msg = ("(starting) {0}@{1} is not running. ").\
                      format(process_name, server)
                if i != (start_attempts - 1): # not last attempt
                    msg += (("Waiting {0} seconds to check again.").\
                            format(start_ch))
                    logger.info(msg)
                else: # last attemp, server was not started
                    msg += (("Max attempts reached ({0}))!!").\
                            format(start_attempts))
                    logger.warning(msg)
                    return False
            else: # server was brought up
                logger.info(("(starting) {0}@{1} was started up.").\
                            format(process_name, server))
                return True
    else:
        logger.info("(starting) {0}@{1} was already running.".\
                    format(process_name, server))
        return True
    return False

def _servers_list(servers_line):
    """Return the list of servers defined by 'servers_line'.

    Keyword arguments:
    servers_line -- if this is a list, just return it. If it is a dictionary, 
    it means the servers list is defined in a bash script, which complete path 
    is the value, and the variable inside the script is the key:
        { 'var_inside_script': 'complete_path2script' }
    Also it's possible to define a dynamic variable inside the name of the 
    variable, like this:
        { '{inner_dynamic_var}_trail_name}': 'complete_path2script' }
    This is useful for refering to the primary data center, or any other 
    specific dynamic variable in the bash script.
    """
    if isinstance(servers_line, dict):
        variable = next(iter(servers_line))
        script_path = servers_line[variable]
        inner_vars = util.get_values2format(variable)
        bash_vals = {}
        for innner_var in inner_vars:
            bash_vals[innner_var] = conn.val_from_bash(innner_var, 
                                                       bashscript=script_path)
        final_var = variable.format(**bash_vals)
        servers_line = conn.val_from_bash(final_var, split=' ', 
                                          bashscript=script_path)
    return servers_line

def _worker(server_line, comp_conf, app_conf):
    msg = "Thread created for line of servers: {0}".format(server_line)
    interval_before_start = app_conf['wait_before_start']
    interval_between_servers = app_conf['interval_between_servers']
    servers_line = _servers_list(server_line)
    if server_line != servers_line:
        msg += (" ==>> {0}".format(servers_line))
    logger.info(msg)
    for server in servers_line:
        start_time = datetime.now()
        if _stop_server(server, comp_conf, app_conf) or \
            _kill_server(server, comp_conf):
            sleep(interval_before_start)
            if not _start_server(server, comp_conf, app_conf):
                not_started_servers.append(server)
            else:
                finish_time = datetime.now()
                total_minutes = util.get_elapse_mins(start_time, finish_time)
                processed_servers.append({server: {
                    'minutes': total_minutes,
                    'start': start_time.strftime('%Y-%m-%d %H:%M:%S'),
                    'finish': finish_time.strftime('%Y-%m-%d %H:%M:%S')
                }})
        else:
            not_stopped_servers.append(server)
        sleep(interval_between_servers)

def _bounce_servers(component, comp_conf, app_conf):
    servers = comp_conf['servers']
    if component in app_conf['component_timing']:
        timing_data = app_conf['component_timing'][component]
        conf_str = component
    else:
        timing_data = app_conf['default_timing']
        conf_str = 'default'
    logger.info("Using '{0}' timing configuration for component {1}.".\
        format(conf_str, component))
    pool = Pool(len(servers))
    for server_line in servers:
        pool.apply_async(_worker, (server_line, comp_conf, timing_data))
    pool.close()
    pool.join()

def _format_processed_servers(servers, html=False):
    """Create and return formatted data for sending by email.

    Keyword arguments:
    servers -- Dict in which the server name is the key, and the value is the 
    server parameters to format.
    html -- Indicates if format is in HTML.
    """
    processeds = [] if html else ""
    for server in servers:
        server_name = next(iter(server))
        st = server[server_name]['start']
        ft = server[server_name]['finish']
        mins = server[server_name]['minutes']
        if html:
            server_data = []
            server_data.append(server_name)
            server_data.append(st)
            server_data.append(ft)
            server_data.append("{0} {1}".format(mins, 'minutes'))
            processeds.append(server_data)
        else:
            tmplt = "{server}\t({start}\t{finish}\t{minutes} minutes)\n"
            processeds += (tmplt.format(server=server_name, start=st, 
                                        finish=ft, minutes=mins))
    if html:
        columns = ["Server", "Start date-time", "Finish date-time", 
                   "Elapsed time"]
        return util.tabled_html_data(processeds, columns, enablesort=True)
    return processeds

def _email_result(component, comp_conf, app_conf, dtstartedp, dtfinishedp):
    """Prepare and send emails informing the servers that were successfully 
    recycled, and an email advising the servers were not 'totally' bounced.
    """
    logger.info("Component: {0}. Notifying by email...".format(component))
    if not_started_servers or not_stopped_servers or processed_servers:
        if 'app_name' in comp_conf and comp_conf['app_name']:
            app_name = comp_conf['app_name']
        else:
            app_name = comp_conf['process_name']
        process_name = comp_conf['process_name']        
        try:
            from pssapi.emailer import emailer
            if not_started_servers or not_stopped_servers:
                wrn_em = app_conf['emailing']['warning']
                subject = wrn_em['subject'].format(app_name)
                sender = wrn_em['sender']
                dests = wrn_em['dests']
                nstops = "\n".join(not_stopped_servers)
                nstarts = "\n".join(not_started_servers)
                body = wrn_em['body'].format(component, process_name, nstops, 
                                             nstarts)
                priority = wrn_em['high_priority']
                save_email = wrn_em['save_email']
                res = emailer.send_email(subject=subject, body=body, 
                                    sender=sender, dest=dests, 
                                    saveit=save_email, highpriority=priority)
                if res == "OK":
                    logger.info(("Email was sent to {0} for advising about " + 
                        "non-bounced servers for component {1} ({2})").\
                            format(dests, component, process_name))
                else:
                    logger.error(("Error when trying to send email to {0} " + 
                        "for advising about non-bounced servers for " + 
                        "component {1} ({2}): {3}").\
                            format(dests, component, process_name, res))
            if processed_servers:
                dtstartedp_str = dtstartedp.strftime('%Y-%m-%d %H:%M:%S')
                dtfinishedp_str = dtfinishedp.strftime('%Y-%m-%d %H:%M:%S')
                inf_em = app_conf['emailing']['info']
                subject = inf_em['subject'].format(app_name)
                sender = inf_em['sender']
                dests = inf_em['dests']
                html = inf_em['html']
                processeds = _format_processed_servers(processed_servers, html)
                body_template = inf_em['body'].replace("\n", "<br />") \
                                if html else inf_em['body']
                elapsed_time = util.get_elapsed_time(dtstartedp, dtfinishedp, 
                                                     includevals="hm")
                body = body_template.format(component, process_name, 
                                            dtstartedp_str, dtfinishedp_str, 
                                            elapsed_time, processeds)
                priority = inf_em['high_priority']
                save_email = inf_em['save_email']
                res = emailer.send_email(subject=subject, body=body, 
                                         sender=sender, dest=dests, 
                                         saveit=save_email, 
                                         highpriority=priority, html=html)
                if res == "OK":
                    logger.info(("Email was sent to {0} for advising the " + 
                        "recycling process for component {1} ({2}) is done.").\
                            format(dests, component, process_name))
                else:
                    logger.error(("Error when trying to send email to {0} " + 
                        "for advising the recycling process for " + 
                        "component {1} ({2}) is done: {3}").\
                            format(dests, component, process_name, res))
        except Exception as e:
            logger.error(("Trying to email result for component {0} ({1}), " + 
                "this error happened: {2}").\
                    format(component, process_name, str(e)))
    else:
        logger.info("Component {0}. Nothing to notify.".format(component))

def recycle(component):
    """Recycle servers defined on <component>.json file."""
    error_message = None
    try:
        dt_process_started = datetime.now()
        comp_conf = _get_component_configuration(component)
        if isinstance(comp_conf, str):
            quit_script(component, comp_conf)
        app_conf = conf.get_conf(APP_CONF_FILE_DIR)
        _check_configuration(component, comp_conf)
        _bounce_servers(component, comp_conf, app_conf)
        dt_process_finished = datetime.now()
        _email_result(component, comp_conf, app_conf, dt_process_started, 
                    dt_process_finished)
        logger.info("Component: {0}. Finished processing.".format(component))
    except Exception as e:
        error_message = str(e)
    quit_script(component, error_message=error_message)
